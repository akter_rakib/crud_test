package com.moyeen.crud_test.service;

import com.moyeen.crud_test.model.Student;

public interface StudentService {

    public void saveStudent(Student student);

}
