package com.moyeen.crud_test.controller;

import com.moyeen.crud_test.model.Student;
import com.moyeen.crud_test.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Autowired
    StudentService studentService;

    @GetMapping(value = "/hello")
    public String hello() {
        return "Hello Moyeen";
    }

    @PostMapping(value = "/saveStudent")
    public Student saveStudent(@RequestBody Student student) {
        studentService.saveStudent(student);
        return student;
    }
}
